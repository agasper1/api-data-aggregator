const https = require('https');
const {
    readFileSync
} = require('fs');
const {
    resolve
} = require('path');

const PATH_TO_CERT = resolve(__dirname, '..', 'certs');

const options = {
    key: readFileSync(`${PATH_TO_CERT}/www.mock-site.com.key`),
    cert: readFileSync(`${PATH_TO_CERT}/www.mock-site.com.crt`)
};

const requestListener = (req, res) => {
    console.log('httpsServer.js - req', req);
    res.writeHead(200);
    res.end('https-server.js\n');
};

const httpsServer = https.createServer(options, requestListener);
httpsServer.listen(3002);