namespace Aggregator {
  class Message {
    // A message consists of two parts
    // 1. Header - Information used by the messaging system that describes the data being transmitted, its origin, its destination, and so on.
    // 2. Body - The data being transmitted, which is generally ignored by the messaging system and simply transmitted as is.
    header: {
      correlationId: number;
      context: string;
      origin: string;
      destination: string;
    };
    body: {
      data: any;
    };
    /**
     * @name getStringProperty
     * @param obj
     * @param key
     */
    getStringProperty<T, K extends keyof T>(obj: T, key: K) {
      return obj[key];
    }
  }
  // Based on JMS Messages - the header structure is the same for all subtypes, but it is the body that differs
  class TextMessage extends Message {
    // the most common type of message
    // body = string, examples: literal text or an XML document
    // the body's contents are read as a string like so:
    // textMessage.getText()
  }
  class BytesMessage extends Message {
    // simplest, most universal type of message
    // body = byte array
    // the body's contents are copied into the specified array
    // byteMessage.readBytes(byteArray)
  }
  class ObjectMessage extends Message {
    // body = single java object that implemented java.io.Serializable
  }
  class StreamMessage extends Message {
    // body = stream of Java Primitives
    // receiver uses methods like the following to read the data from the message:
    // readBoolean()
    // readChar()
    // readDouble()
  }
  class MapMessage extends Message {
    // body acts like java.util.Map
    // methods like:
    // getBoolean("isEnabled");
    // getInt("numberOfItems");
    // are used to read data from the message
    getBoolean: (property: string) => boolean;
    getInt: (property: number) => number;
  }
  interface Session {
    createConsumer(destinaton: MessageDestination): MessageConsumer;
    createProducer(destination: MessageDestination): MessageProducer;
    createMapMessage(): MapMessage;
  }
  interface Aggregate {
    addMessage: (message: Message) => void;
    isComplete: () => boolean;
    getResultMessage: () => Message;
  }

  class Entity {
    id: number;
    name: string;
    domainAttribute: any;
    getProperty<T, K extends keyof T>(obj: T, key: K): T[K] {
      return obj[key];
    }
  }
  class Order extends Entity {
    id: 1;
    name: "order name";
    products: [{}, {}, {}];
  }
  class Domain {
    entities: Entity[];
    constructor() {}
    public addEntity(entity: Entity): void {
      this.entities.push(entity);
      return;
    }
    public isComplete(): boolean {
      // completeness logic
      return true;
    }
    public getBestEntity(): Entity {
      let bestEntity: Entity = null;
      // sloppy - this would
      this.entities.map((entity, index) => {
        if (index === 0) {
          bestEntity = entity;
        }
        // arbitrary comparison for now
        if (
          entity.getProperty(entity, "domainAttribute") >
          bestEntity.getProperty(bestEntity, "domainAttribute")
        ) {
          bestEntity = entity;
        }
      });
      return bestEntity;
    }
  }
  interface MessageDestination {
    // Abstraction for either:
    // a Queue (Point-to-Point Channel)
    // or
    // a Topic (Publish-Subscribe Channel)
    // testing - pub-sub topics => listen in on message traffic
    // production - queue
  }

  interface MessageConsumer {
    /**
     * @name setMessageListener
     * @param {MessageListener} listener - the listener to which the messages are to be delivered
     */
    setMessageListener: (listener: MessageListener) => void;
  }

  interface MessageProducer {
    send: (message: Message) => void;
  }

  interface MessageListener {
    /**
     * @name onMessage
     * @param {Message} message - the message passed to the listener
     * @description - Passes a message to the listener
     */
    onMessage: (message: Message) => void;
  }

  interface AggregatorProperties {
    CORRELATION_ID: string;
    activeAggregates: HashMap;
    inputDestnation: MessageDestination;
    outputDestination: MessageDestination;
    session: Session;
    in: MessageConsumer;
    out: MessageProducer;
  }

  interface IHash {
    [id: string]: string;
  }
  class HashMap {
    private keys: IHash = {};
    private values: string[] = [];
    public add(key: string) {
      if (!this.keys[key]) {
        this.values.push(key);
        this.keys[key] = key;
      }
    }
    public get(key: string) {
      if (this.keys[key]) {
        return this.values[key];
      } else {
        return null;
      }
    }
  }
  class DomainAggregate implements Aggregate {
    CORRELATION_ID: string = "ConcreteAggregate";
    ITEMID: string = "ItemID";
    VENDOR: string = "Vendor";
    PRICE: string = "Price";

    private session: Session;
    private domain: Domain;

    public DomainAggregate(session: Session) {
      this.session = session;
      this.domain = new Domain();
    }
    public addMessage(message: Message): void {
      let entity: Entity = null;
      if (message instanceof MapMessage) {
        try {
          const mapMessage = MapMessage,
            message;
          const domainId: string = mapMessage.getStringProperty(
            "header",
            this.CORRELATION_ID
          );
          // get message properties
          // getCorrelationId
          // getItemId
          // getVendor
          // getPrice
          // enttiy = new Entity(...messageAttributes);
          // doman.addEnttiy(entity);
        } catch (e) {}
      }
    }
    public isComplete(): boolean {
      return this.domain.isComplete();
    }
    public getResultMessage(): Message {
      const bestEntity = this.domain.getBestEntity();
      try {
        let message: Message = this.session.createMapMessage();
        // message.setStringProperty('correlationId', bestEntity.getId();
        // message.setStringProperty('id', bestEntity.getId())
        // message.setStringProperty('vendor', bestEntity.getVendorId());
        // message.setStringProperty('price', bestEntity.getPrice());
        return message;
      } catch (e) {
        return null;
      }
    }
  }

  class AggregateFactory implements Aggregate {
    constructor() {}
    createAggregate(): Aggregate {
      return new DomainAggregate();
    }
    addMessage: (message: Message) => void;
    isComplete: () => boolean;
    getResultMessage: () => Message;
  }

  export class Aggregator implements MessageListener {
    // fields
    CORRELATION_ID: string;
    activeAggregates: HashMap;
    inputDestnation: MessageDestination;
    outputDestination: MessageDestination;
    session: Session;
    in: MessageConsumer;
    out: MessageProducer;
    // constructor
    constructor(props: AggregatorProperties) {
      this.CORRELATION_ID = props.CORRELATION_ID;
      this.activeAggregates = props.activeAggregates;
      this.inputDestnation = props.inputDestnation;
      this.outputDestination = props.outputDestination;
      this.session = props.session;
      this.in = props.in;
      this.out = props.out;
    }
    // functions
    /**
     * @name AggregatorInit
     * @param {MessageDestination} inputDestination
     * @param {MessageDestination} outputDestination
     * @param {Session} session
     * @description - Set the input, output, and session values for the aggregator
     * @return {void}
     */
    AggregatorInit(
      inputDestination: MessageDestination,
      outputDestination: MessageDestination,
      session: Session
    ): void {
      this.inputDestnation = inputDestination;
      this.outputDestination = outputDestination;
      this.session = session;
      return;
    }
    public run(): void {
      try {
        this.in = this.session.createConsumer(this.inputDestnation);
        this.out = this.session.createProducer(this.out);
      } catch (e) {}
      return;
    }
    onMessage(message: Message): void {
      try {
        const correlationId = message.getStringProperty(
          "header",
          this.CORRELATION_ID
        );
        let aggregate: Aggregate = this.activeAggregates.get(correlationId);
        if (aggregate === null) {
          const aggregateFactory = new AggregateFactory();
          const aggregate = aggregateFactory.createAggregate();
          this.activeAggregates.add(correlationId);
        }
        // create a new aggregate for the message

        // if aggregation is not complete then add the message
        if (!aggregate.isComplete()) {
          aggregate.addMessage(message);
          const result: MapMessage = aggregate.getResultMessage();
          this.out.send(result);
        }
      } catch (e) {}
      return;
    }
  }
}

// factory Pattern for
