const {
    createServer,
    Agent,
    request
} = require('https');
const querystring = require('querystring');
const {
    readFileSync
} = require('fs');
const {
    resolve
} = require('path');
const PATH_TO_CERT = resolve(__dirname, 'certs');

const amounts = [5.00, 10.00, 20.00, ]
const zipcodes = [1, 2, 3];
const skus = ['abc123', 'def345', 'ghi789'];
const taxes = {};

/**
 * @name createPostData
 * @param {float} amount 
 * @param {string} zipcode 
 * @param {string} sku 
 * @description - Create querystring version of the post body
 * @return {string} - Post data as querystring 
 */
function createPostData(amount, zipcode, sku) {
    return querystring.stringify({
        amount,
        zipcode,
        sku
    });
}

function taxRequest(amount, zipcode, sku) {
    console.log('taxRequest');
    return new Promise((resolve, reject) => {
        const postData = createPostData(amount, zipcode, sku);
        // create the agentOptions with no verify
        const options = {
            hostname: 'www.mock-site.com',
            port: 3002,
            path: '/',
            method: 'POST',
            agent: requestAgent,
            timeout: 1000,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Content-Length': Buffer.byteLength(postData)
            },
            rejectUnauthorized: false,
        };
        let chonks = [];
        const req = request(options, (res) => {
            console.log(`STATUS: ${res.statusCode}`);
            console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
            res.setEncoding('utf8');
            res.on('data', (chunk) => {
                console.log(`BODY: ${chunk}`);
                chonks.push(chunk);
            });
            res.on('end', () => {
                console.log('No more data in response.');
                resolve({
                    'message': 'responseEnd',
                    chonks
                })
            });
        });
        // REQUEST ERROR HANDLING
        req.on('error', (e) => {
            // end the request 
            // req.end();
            console.error(`problem with request: ${e.message}`);
            reject({
                'message': 'error',
                e
            });
        });
        req.end();
    });
}

/**
 * @name backoff
 * @param {int} time - Time in seconds
 */
function backoff(time) {
    let milliseconds = time * 1000;
    const start = (new Date()).getTime();
    while ((new Date()).getTime() - start < milliseconds) {
        console.log('still waiting');
    }
    return;
}
// create the httpsServer for the sender site

const requestListener = (req, res) => {
    console.log('requestListener - server.js');

    (async function () {
        try {
            const result = await taxRequest(1, 2, 3);
            console.log('inside of try block - result:', result);
            res.writeHead(200);
            res.write(JSON.stringify(result));
            res.end();
        } catch (error) {
            // console.error('error inside of server.js', error);
            if (error.e) {
                res.writeHead(521); // a la CloudFlare
                res.write(JSON.stringify({
                    'error': 'connection refused'
                }));
                res.end();
            } else {
                res.writeHead(520);
                res.write(JSON.stringify({
                    'error': 'vague error'
                }));
                res.end();
            }
        }
    })();
}

const agentOptions = {
    timeout: 1000,
    rejectUnauthorized: false,
};
const requestAgent = new Agent(agentOptions);
const serverOptions = {
    key: readFileSync(`${PATH_TO_CERT}/www.sender-site.com.key`),
    cert: readFileSync(`${PATH_TO_CERT}/www.sender-site.com.crt`),
    agent: requestAgent,
    timeout: 1000,
};
const httpsServer = createServer(serverOptions, requestListener);
httpsServer.listen(3003);
process.on('unhandledRejection', (reason, p) => {
    const debugging = {
        reason,
        p
    };
    console.log('debugging', debugging);
    console.log(`unhandledRejection at: Promise - ${p}; reason - ${reason}`);
});